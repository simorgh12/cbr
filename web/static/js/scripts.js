/**
 * Created by pablo on 19/01/2017.
 */

var sandwich = null;
var desired = null;
var undesired = null;


storeResult = function storeResult(res) {
    sandwich = res;
    console.log(sandwich)
};

onClickSend = function onClickSend() {
    desired = $('#desiredInput').val();
    undesired = $('#undesiredInput').val();

    var data = {
        desired: (desired.toLowerCase()).split(', '),
        undesired: (undesired.toLowerCase()).split(', '),
    };

    $.ajax({
        url: "/api/predict",
        type: "POST",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            console.log(data);
            getRandomImage(data.name);
            storeResult(data);
            createResultPage(data)
        }
    });
};

onClickEdit = function onClickEdit() {
    var html = "Edit the following fields:";
    html += "<form id='sandwichForm'>";
    for (var i = 0; i < sandwich.recipe.length; i++) {
        html += '<input type="text" class="form-control elaborationInput" value="';
        html += sandwich.recipe[i];
        html += "\"></input>";
    }
    html += "</form>";
    console.log(html);

    $("#resultRecipe").html(html);

    html = '<input id="nameInput" type="text" class="form-control" value="';
    html += sandwich.name;
    html += "\"></input>";
    $("#resultTitle").html(html);
    $("#resultAction").html(getEditCancelButton());
};

onClickRevise = function onClickRevise() {
    var desired = $('#desiredInput').val();
    var undesired = $('#undesiredInput').val();
    var recipe = [];
    for (var i = 0; i < sandwich.recipe.length; i++) {
        recipe.push($('.elaborationInput')[i].value);
    }
    var ingredients = sandwich.ingredients;
    sandwich.name = $('#nameInput').val();
    var title = sandwich.name;
    console.log("TITLE"+title);

    var data = {
        desired: (desired.toLowerCase()).split(', '),
        undesired: (undesired.toLowerCase()).split(', '),
        title: title,
        ingredients: ingredients,
        recipe: recipe
    };

    $.ajax({
        url: "/api/revise",
        type: "POST",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            //STUB
            sandwich.recipe = recipe;
            console.log(data.stored);
            var result = data.stored ? -1 : -2;
            createResultPage(result);
        }
    });
};

createResultPage = function createResultPage(edit) {
    //$("#resultTitle").text('The recommendend sandwich is...');

    var html = sandwich.name;
    $('#resultTitle').html(html);
    var i;
    html="<ul>";
    for (i = 0; i < sandwich.ingredients.length; i++) {
        html += "<li>" + sandwich.ingredients[i] + "</li>";
    }
    html += "</ul>";
    $("#resultIngredients").html(html);

    html = "<ol>";
    for (i = 0; i < sandwich.recipe.length; i++) {
        html += "<li>" + sandwich.recipe[i] + "</li>";
    }
    html += "</ol>";
    $('#resultRecipe').html(html);

    if (edit != -1 && edit != -2){
        $('#resultAction').html(getReviseButton());
    } else if(edit == -1){
        $('#resultAction').html('<div class="alert alert-success" role="alert"> <strong>Well done!</strong> Your recipe has been revised. </div>');
    } else if(edit == -2){
        $('#resultAction').html('<div class="alert alert-warning" role="alert"> <strong>Well done!</strong> Even though your sandwich was already in the database</div>');
    }

    $('#result').removeAttr('hidden');
    goToByScroll("result")
};


goToByScroll = function goToByScroll(id) {
    // Remove "link" from the ID
    id = id.replace("link", "");
    // Scroll
    $('html,body').animate({
            scrollTop: $("#" + id).offset().top
        },
        'slow');
};

getEditCancelButton = function getEditCancelButton() {
    return '<button id="buttonEdit" type="button" class="btn btn-lg btn-info" style="margin-top: 20px" onclick="onClickRevise()">Revise!</button> ' +
        '<button id="buttonCancel" type="button" class="btn btn-lg btn-danger" style="margin-top: 20px" onclick="createResultPage()">Cancel</button>'
};

getReviseButton = function getReviseButton() {
    return '<button id="buttonEdit" type="button" class="btn btn-lg btn-success" style="margin-top: 20px" onclick="onClickEdit()">Revise the elaboration</button>'
};

getRandomImage = function getRandomImage(keyword){
    console.log(keyword);
    $.getJSON("https://api.flickr.com/services/rest/?method=flickr.photos.search&" +
        "sort=relevance&" +
        "api_key=5f33b2ecea05f95495fe5d584a621496&" +
        "tags=sandwich" +
        "&text=" + encodeURIComponent(keyword) +
        "&format=json&nojsoncallback=1",
        function(data) {
            if (data['photos']['photo'].length == 0) {
                $('#sandwichImage').attr('src', "");
                return
            }
            var image_src = data['photos']['photo'][0];
            var url = "https://farm{farm-id}.staticflickr.com/{server-id}/{id}_{secret}.jpg"
                .replace("{farm-id}", image_src['farm'])
                .replace("{server-id}", image_src['server'])
                .replace("{id}", image_src['id'])
                .replace("{secret}", image_src['secret']);
            $('#sandwichImage').attr('src', url);

        });
};