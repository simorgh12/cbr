# -*- coding: utf-8 -*-
"""
Created on Tue Jan 18 23:31:06 2017

@author: Vicent Roig
"""

from BeautifulSoup import BeautifulSoup
import urllib2
import xml.etree.ElementTree as ET

def prettify(elem):
    """Return a pretty-printed XML string for the Element.
    """
    rough_string = tostring(elem, 'utf-8')
    reparsed = minidom.parseString(rough_string)
    return reparsed.toprettyxml(indent="\t")

class FoodCrawler:
    def __init__(self):
        self.backup = 'http://web.archive.org'
        self.url = "/web/20160516005944/http://wikitaaable.loria.fr/index.php/Category:Food"
        self.root = ET.Element('Food')
        self.tree = None
        self._visited = []
        
        
    def start(self, verbose=False):
        # reset record
        self._visited = []
        
        # recursively scraping data process
        self.step(self.backup + self.url, self.root, verbose)

        # save intro XML file
        self.saveTree()
        
        
    def step(self,url,parent,verbose=False):
        try:
            html_page = urllib2.urlopen(url)
        except urllib2.HTTPError as err:
            return

        soup = BeautifulSoup(html_page)
        categoryDivs = soup.findAll('div', attrs={'class':'CategoryTreeSection'})
        for div in categoryDivs:
            link = self.backup + div.find('a')['href']
            category = link.split('/')[-1].split(':')[-1].replace('_',' ')
            if category not in self._visited: # avoid cycles
                self._visited.append(category)
                if verbose:
                    print category,'\n',link
                
                child = ET.SubElement(parent,'Subcategory')
                child.text = category

                self.step(link,child,verbose)
                
    def getTree(self):
        return ET.ElementTree(self.root)
    
    def saveTree(self,path=''):
        """
        dump XML tree content into a file in the specified path location.
        """
        tree = ET.ElementTree(self.root)
        tree.write(path+"./data/ccc-ingredients.xml")
        
    