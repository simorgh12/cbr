# -*- coding: utf-8 -*-
"""
Created on Tue Dec 20 16:23:27 2016

@author: Chris, Pablo Martínez, Vicent Roig, Aleix Solanes
"""

import urllib

import xml.etree.ElementTree as et
import warnings
from difflib import SequenceMatcher


def build_cbr():
    # Load the recipes XML
    recipes_xml = load_xml('./data/ccc_sandwich.xml')

    # Create the problems
    solutions = []
    for i in recipes_xml:
        solutions.append(Solution(i))

    # Get the list of all ingredients in the recipes
    all_ingredients = []
    for i in solutions:
        for j in i.ingredients:
            if j not in all_ingredients:
                all_ingredients.append(j)

    # Create the CBR object
    cbr = CBR(solutions, all_ingredients)
    cbr.initialize([x.ingredients for x in solutions])
    return cbr


def load_xml(path):
    return et.parse(path).getroot()


def compare_str(str1, str2):
    return SequenceMatcher(None, str1, str2).ratio()


def replace_word(string, word, replace, threshold):
    string_list = string.split()
    word_list = word.split()
    found_flag = False
    for w_idx in range(len(string_list)):
        # print "String comparison between: ", word, " and ", string_list[w_idx],
        # " = ", compare_str(word, string_list[w_idx])
        compare_score = 0
        c = 1
        if compare_str(word_list[0], string_list[w_idx]) > threshold:
            # print "Replacing: ", string_list[w_idx], " with ", replace
            string_list[w_idx] = replace
            found_flag = True
            if len(word_list) > 1:
                for i in range(1,len(word_list)):
                    if w_idx+i < len(string_list):
                        del(string_list[w_idx+i])
                
            break
    if found_flag:
        return " ".join(string_list)
    else:
        return []

"""
BASIC CLASSES
"""


class Problem:
    """
    The class Problem is an instance of a problem that contains wanted and unwanted ingredients
    """

    def __init__(self, desired_ingredients, unwanted_ingredients):
        assert type(desired_ingredients) == list and type(unwanted_ingredients) == list

        self._desired_ingredients = desired_ingredients
        self._unwanted_ingredients = unwanted_ingredients

    def __str__(self):
        return str([self.get_desired_ingredients(), self.get_unwanted_ingredients()])

    def get_desired_ingredients(self):
        return self._desired_ingredients

    def get_unwanted_ingredients(self):
        return self._unwanted_ingredients

    def compare_desired(self, other):
        """
        Compares the desired ingredients with the desired ingredients of another problem. Returns a match percentage
        :param other:
        :return:
        """
        oth_d_i = other.get_desired_ingredients()
        c = 0
        for x in self._desired_ingredients:
            if x in oth_d_i:
                c += 1
        if c == 0:
            return 0
        return c / float(len(self._desired_ingredients))

    def compare_desired_undesired(self, other):
        """
        Compares the desired ingredients with the desired ingredients of another problem. Returns a match percentage
        :param other:
        :return:
        """
        oth_d_i = other.get_unwanted_ingredients()
        c = 0
        for x in self._desired_ingredients:
            if x in oth_d_i:
                c += 1
        if c == 0:
            return 0
        return c / float(len(self._desired_ingredients))

    def compare_undesired_desired(self, other):
        """
        Compares the desired ingredients with the desired ingredients of another problem. Returns a match percentage
        :param other:
        :return:
        """
        oth_d_i = other.get_desired_ingredients()
        c = 0
        for x in self._unwanted_ingredients:
            if x in oth_d_i:
                c += 1
        if c == 0:
            return 0
        return c / float(len(self._desired_ingredients))

    def compare_undesired(self, other):
        """
        Compares the undesired ingredients with the desired ingredients of another problem. Returns a match percentage
        :param other:
        :return:
        """
        oth_u_i = other.get_unwanted_ingredients()
        c = 0
        for x in self._unwanted_ingredients:
            if x in oth_u_i:
                c += 1
        if c == 0:
            return 0
        return c / float(len(self._unwanted_ingredients))

    def compare(self, other):
        # TODO: Improve this metric
        return self.compare_desired(other) + self.compare_undesired(other) \
               - self.compare_desired_undesired(other) - self.compare_undesired_desired(other)


class Solution:
    @staticmethod
    def contains_list(arr1, arr2):
        for i in arr1:
            if not i in arr2: return False
        return True

    def __init__(self, args):
        if type(args) == list:
            self.title = args[0]
            self.ingredients = args[1]
            self.recipe = args[2]

        else:
            # ingredients and preparation are both list of strings
            self.title = args[0].text
            self.ingredients = []
            self.recipe = []

            for ingredient in args[1]:
                self.ingredients.append(ingredient.attrib['food'].lower())
            for step in args[2]:
                self.recipe.append(step.text)

    def __str__(self):
        s = ''
        s += '====' + self.title + '====\n'
        for ingredient in self.ingredients:
            s += '+' + ingredient + '\n'
        for idx, step in enumerate(self.recipe):
            s += str(idx) + ' ' + step + '\n'
        return s

    def get_title(self):
        return self.title

    def get_ingredients(self):
        return self.ingredients

    def get_recipe(self):
        return self.recipe

    def score(self, ingredient_list):
        i = 0
        for ingredient in self.ingredients:
            if ingredient in ingredient_list:
                i += 1
        return i

    def compare(self, other):
        nn = 0
        for ingredient in self.ingredients:
            if ingredient in other.ingredients:
                nn += 1

        return nn / (len(self.ingredients) + len(other.ingredients) - nn)
        
        
class KnowledgeBase:
    """
    The Knowledge Base class contains the knowledge base and the methods needed to manipulate the knowledge
    """
    def __init__(self, knowledge_base_file, threshold, max_cat = 4):
        self.threshold = threshold
        self.kb = load_xml(knowledge_base_file)
        self.max_cat =max_cat
        
        # shave knowledge base
        for f_idx in range(len(self.kb)):
            for s_idx in range(len(self.kb[f_idx])):
                for t_idx in range(len(self.kb[f_idx][s_idx][1])):
                    self.kb[f_idx][s_idx][1][t_idx].text = self.kb[f_idx][s_idx][1][t_idx].text.strip().lower()

    def get_category(self, ingredient):
        #print "== get_category =="
        for f_idx in range(len(self.kb)):
            for s_idx in range(len(self.kb[f_idx])):
                lowest_list = self.kb[f_idx][s_idx][1].findall('food')                
                idx = next((i for i in range(len(lowest_list)) if compare_str(lowest_list[i].text, ingredient) > self.threshold), -1)
                
                #for i in range(len(lowest_list)):
                #    print "string comparison between: ", lowest_list[i].text, " and ",  ingredient, " = ", compare_str(lowest_list[i].text, ingredient)
                
                if idx >= 0:
                    # found category of d
                    return [f_idx, s_idx, lowest_list]
        return [] 
        
    def get_ingredient(self, category, undesired):
        for ing in category:
            undes = False
            for u in undesired:
                if compare_str(ing.text, u) > self.threshold:
                    # ing is in undesired list
                    undes = True
                    break
            if not undes:
                # ing is not undesired
                return ing.text
            
        # no ingredient found that was not undesired
        return []

    def is_in_category(self, category, ingredients, blocked = []):
        #print "== is_in_category =="
        for ing_idx in range(len(ingredients)):
            if ing_idx in blocked:
                continue
            
            #for i in range(len(category)):
            #    print "string comparison between: ", ingredients[ing_idx], " and ",  category[i].text, " = ", compare_str(ingredients[ing_idx], category[i].text)
            
            idx = next((i for i in range(len(category)) if compare_str(ingredients[ing_idx], category[i].text) > self.threshold), -1)
            if idx >= 0:
                #found ingredient of same category
                return ing_idx
        return -1  

    def idx2cat(self, category_indces):
        return self.kb[category_indces[0]][category_indces[1]][1].findall('food') 
        
    def get_close_categories(self, category_indces):
        diet_diffs = []
        cat_indcs = []
        cat_diet = len(self.kb[category_indces[0]][category_indces[1]][0])
       
        for s_idx in range(len(self.kb[category_indces[0]])):
            if not (s_idx == category_indces[1]): 
                diet_diffs.append(abs(len(self.kb[category_indces[0]][s_idx][0]) - cat_diet))
                cat_indcs.append([category_indces[0], s_idx])
        
        return [ci for (dd,ci) in sorted(zip(diet_diffs,cat_indcs))]

    def get_tag(self, category_indces):
        return self.kb[category_indces[0]][category_indces[1]].tag
    

class Case:
    """
    Case is a class that contains a problem and the solution. (just a wrapper)
    """

    def __init__(self, problem, solution):
        assert problem.__class__ == Problem and solution.__class__ == Solution
        self._problem = problem
        self._solution = solution

    def get_problem(self):
        return self._problem

    def get_solution(self):
        return self._solution


class CaseBase:
    def __init__(self, sols):
        assert type(sols) == list
        assert all([x.__class__ == Solution for x in sols])

        """
        When case base is initializated it creates the problems that fits perfectly the solution
        :param sols:
        """
        self.cases = []
        for sol in sols:
            self.cases.append(Case(Problem(sol.get_ingredients(), []), sol))

    def add_case(self, case):
        self.cases.append(case)

    def get_knn(self, k, problem):
        score = [(x.get_problem().compare(problem), x) for x in self.cases]
        score.sort(reverse=True)
        return [x[1] for x in score[:k]]


class CBR:
    def __init__(self, solutions, ingredients, threshold = 0.8):
        self.solutions = solutions
        self.threshold = threshold
        self.case_base = CaseBase(solutions)
        self.kb = KnowledgeBase('./data/kb.xml', self.threshold)
        
    # def initialize(self, problems):
    #     """
    #     Receives a list of problems and generates the case base, matching each case with the most similar solution.
    #     :param problems: List of lists of ingredients: [['Ham', 'Burguer'], ['Bread','Cucumber', 'Oil], ...]
    #     :return:
    #     """
    #     for problem in problems:
    #         p = Problem(problem[0], problem[1])
    #         l = [(x.retrieve_score(problem), x) for x in self.solutions]
    #         l.sort(key=lambda x: x[0])
    #         c = Case(p, l[-1][1])
    #         self.case_base.append(c)
    #     return self.case_base

    def solve(self, problem):
        '''
        DEPRECATED - Use predict instead
        :param problem:
        :return:
        '''
        assert problem.__class__ == Problem
        warnings.warn('deprecated', DeprecationWarning)
        """
        Main method
        :param problem: a Problem object
        :return:
        """
        old_problem, solution = self.__retrieve__(problem)
        solution, replace = self.__reuse__(problem, solution)
        revised_solution = self.__revise__(problem, solution, replace)
        self.__retain__(problem, revised_solution)

    def predict(self, problem):
        assert self.case_base
        retrieved_problem, retrieved_solution = self.__retrieve__(problem)
        reused_solution = self.__reuse__(retrieved_problem, retrieved_solution)
        return reused_solution

    def revise(self, problem, revised_solution):
        assert problem.__class__ == Problem
        assert revised_solution.__class__ == Solution
        return self.__retain__(problem, revised_solution)
        
    def sub_desired(self, desired, desired_indces, solution, kb):
        """
        Substitutes the desired ingredient with the best match in ingredients
        :param desired: list of desired in  gredients that should be included in the solution
        :param desired_indces: index of desired ingredients allready in the solution 
        return: 
        """
        blocked = desired_indces
        for d in desired:
                       
            # search category of d
            category_list = kb.get_category(d)
            if not category_list:
                # No knowledge on desired ingredient
                print "Desired ingredient: ", d, " not found in knowledge base"
                
                solution.recipe.append("Add " + d)
                solution.ingredients.append(d)
                blocked.append(len(solution.ingredients)-1)
                
                continue
            
            cat_indces = category_list[0:2]
            category = category_list[2]
           
            # search for ingredients of the same category
            sub_idx = kb.is_in_category(category, solution.ingredients, blocked)
            if sub_idx >= 0:
                # substitute found
                
                # change preparation
                for step_idx in range(len(solution.recipe)):
                    new_string = replace_word(solution.recipe[step_idx], solution.ingredients[sub_idx], d, self.threshold)
                    if new_string:
                        solution.recipe[step_idx] = new_string
                
                # change ingredients
                solution.ingredients[sub_idx] = d
                blocked.append(sub_idx)
                
            else:
                # No ingredient found in the current category --> get similar categories based on diet
                close_cat_indces = kb.get_close_categories(cat_indces)
                found_flag = False
                for cat_i in close_cat_indces:
                    
                    category = kb.idx2cat(cat_i)
                    # search for ingredients of close category
                    #print "Close Category: ", kb.get_tag(cat_i)
                    #print "Ingredients: ", solution.ingredients
                    #print "Blocked: ", blocked
                    sub_idx = kb.is_in_category(category, solution.ingredients, blocked)
                    if sub_idx >= 0:
                        # substitute found
                        
                        # change preparation
                        for step_idx in range(len(solution.recipe)):
                            new_string = replace_word(solution.recipe[step_idx], solution.ingredients[sub_idx], d, self.threshold)
                            if new_string:
                                solution.recipe[step_idx] = new_string
                            
                        # change ingredients    
                        solution.ingredients[sub_idx] = d
                        blocked.append(sub_idx)  
                        found_flag = True
                                         
                        break
                        
                if not found_flag:
                    # No fitting ingredients found to substitute --> add ingredient to sandwich
                    solution.recipe.append("Add " + d)
                    solution.ingredients.append(d)
                    blocked.append(len(solution.ingredients)-1)
                            
                
        return solution
        
    def sub_undes(self, undesired, undesired_indces, solution, kb):
        """
        Substitute undesired ingredients in the solution with a matching ingredient
        :param undesired: undesired ingredients that should be removed from the solution
        :param undesired_indces: Indices of all the undesired ingredients
        """
        for u_idx in undesired_indces:
            
            # search category of u
            category_list = kb.get_category(solution.ingredients[u_idx])
            if not category_list:
                print "Desired ingredient: ", solution.ingredients[u_idx], " not found in knowledge base"
                del(solution.ingredients[u_idx])
                #blocked.append(len(solution.ingredients)-1)
                continue

            cat_indces = category_list[0:2]
            category = category_list[2]
            
            # get first ingredient that is not undesired from category
            sub = kb.get_ingredient(category, undesired)
            if sub:
                # Substitute was found
                
                # change preparation
                for step_idx in range(len(solution.recipe)):
                    new_string = replace_word(solution.recipe[step_idx], solution.ingredients[u_idx], sub, self.threshold)
                    if new_string:
                        solution.recipe[step_idx] = new_string
                
                # change ingredients
                solution.ingredients[u_idx] = sub
            else:
                # No substitute found in category
                
                #change preparation
                for step_idx in range(len(solution.recipe)):
                    if replace_word(solution.recipe[step_idx], solution.ingredients[u_idx], "", self.threshold):
                        del(solution.recipe[step_idx])
                
                #change ingredients
                del(solution.ingredients[u_idx])
                
                
        return solution
        
    
    def __retrieve__(self, problem, strategy="knn", k=5):
        """
        Given a problem, returns the most similar case stored in the case base.
        :param problem:
        :return: [old_problem, solution] -> where old problem is the retrieved problem from the CB
        """

        # This is a stub.
        if strategy=="knn":
            knn = self.case_base.get_knn(k, problem)
            return problem, knn[0].get_solution()

        return None

    def __reuse__(self, problem, solution):
        """
        Check if the solution contain any item that we don't want in the current case. If we find such item, remove it
        from the recipe and replace it by a similar one, according to the similarity matrix.
        :param case:
        :param solution:
        :return:
        """
        modified = False
        undes = problem.get_unwanted_ingredients()
        des = problem.get_desired_ingredients()
        # check which desired ingredients are not included in the solution
        des_sol = []
        des_idx = []
        for d in des:
            idx = next((i for i in range(len(solution.ingredients)) if compare_str(solution.ingredients[i],d) > self.threshold), -1)
            if idx < 0:
                des_sol.append(d)
            else:
                des_idx.append(idx)
        
        # if desired ingredients are found, substitute them
        if des_sol: 
            modified = True
            solution = self.sub_desired(des_sol, des_idx, solution, self.kb)
             
        # check if solution contains undesired ingredients
        undes_sol = []
        undes_idx = []
        for u in undes:
            sol_idx = next((i for i in range(len(solution.ingredients)) if compare_str(solution.ingredients[i],u) > self.threshold), -1)
            if sol_idx >= 0:
                undes_sol.append(solution.ingredients[sol_idx])
                undes_idx.append(sol_idx)
        
        if undes_sol:
            modified = True
            # undesired ingredients found
            solution = self.sub_undes(undes, undes_idx, solution, self.kb)
            
        # change title
        # if ('(modified)' not in solution.title) and modified:
        #     solution.title += " (modified)"
        
        return solution

    def __revise__(self, problem, solution, replace):
        """
        Simulate the test in the real world replacing the element in the steps of the recipe that contains the unwanted
        ingredients by steps containing modifications made by the user.
        :param case:
        :param solution:
        :param replace:
        :return:
        """
        
        # Check wether the user has proposed any replacement
        if replace is None:
            return solution
        else:
            return replace

    def __retain__(self, problem, revised_solution):
        """
        Store the case in the case base. Check if the case isn't exactly another one.
        :param case:
        :param revised_solution:
        :return:
        """
        
        new_ing = revised_solution.get_ingredients()
        new_rec = revised_solution.get_recipe()
        #print 'new ing:',new_ing
        for sol in self.solutions:
            old_ing = sol.get_ingredients()
            old_rec = sol.get_recipe()

            #print 'old_ing:',old_ing
            # check if ingredients and recipe are the same.
            if all(item in old_ing for item in new_ing) and len(old_ing) == len(new_ing):
                # we already have a sandwich with those ingredients!

                if all(step in old_rec for step in new_rec) and len(old_rec) == len(new_rec):
                    # we already have a sandwich with this ingredients and recipe! (forget)
                    print '\t*** Forgettable sandwich recipe ***'
                    return False
                
        print '\t*** Retaining new sandwich recipe ***'
        self.solutions.append(revised_solution)
        self.case_base.add_case(Case(problem, revised_solution))

        return True


if __name__ == "__main__":
    # Load the recipes XML
    recipes_xml = load_xml('./data/ccc_sandwich.xml')

    # Create the problems
    solutions = []
    for i in recipes_xml:
        solutions.append(Solution(i))
        
    # Get the list of all ingredients in the recipes
    all_ingredients = []
    for i in solutions:
        for j in i.ingredients:
            if j not in all_ingredients:
                all_ingredients.append(j)

    # Create the CBR object
    cbr = CBR(solutions, all_ingredients)
