from flask import Flask, render_template, request, jsonify
import json
from cbrm import cbr

app = Flask(__name__)
# Load the recipes XML
recipes_xml = cbr.load_xml('./data/ccc_sandwich.xml')

# Create the problems
solutions = []
for i in recipes_xml:
    solutions.append(cbr.Solution(i))

# Get the list of all ingredients in the recipes
all_ingredients = []
for i in solutions:
    for j in i.ingredients:
        if j not in all_ingredients:
            all_ingredients.append(j)
            
all_ingredients = [i.lower() for i in all_ingredients]
print all_ingredients

CBR = cbr.CBR(solutions, all_ingredients)

def get_url(file):
    return file


@app.route("/")
def index_view():
    global CBR

    page_name = get_url('index')
    return render_template('%s.html' % page_name, cbr=CBR)

@app.route("/ingredients")
def ingredients_view():
    global all_ingredients

    page_name = get_url('ingredients')
    return render_template('%s.html' % page_name, ingredients=all_ingredients)


@app.route('/api/predict', methods=['POST'])
def predict_case():
    global CBR
    content = request.get_json()

    # Retrieve the parameters
    desired = content.get('desired')
    undesired = content.get('undesired')

    # Create a new problem
    problem = cbr.Problem(desired, undesired)
    reused_solution = CBR.predict(problem)

    name = reused_solution.title
    ingredients = reused_solution.ingredients
    recipe = reused_solution.recipe
    return json.dumps({'name': name, 'ingredients': ingredients, 'recipe': recipe})


@app.route('/api/revise', methods=['POST'])
def revise_case():
    global CBR
    content = request.get_json()

    # Retrieve the parameters
    desired = content.get('desired')
    undesired = content.get('undesired')
    title = content.get('title')
    ingredients = content.get('ingredients')
    recipe = content.get('recipe')

    # Create a new problem
    problem = cbr.Problem(desired, undesired)
    revised_solution = cbr.Solution([title, ingredients, recipe])
    decision = CBR.revise(problem, revised_solution)

    return json.dumps({'stored': decision})

# @app.route('/api/solve', methods=['POST'])
# def add_solution():
#     CBR.

if __name__ == "__main__":
    app.run()
    #CBR = build_cbr()
