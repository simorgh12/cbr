AMLT - Fall semester 2017
=================================================

# Case Based Reasoning Project

This project covers the issue of how to analyse, design, implement and validate a Case-Based Reasoning System (CBRS) for a concrete domain. In particular, it will try to give a solution to the 2015 Sandwich Challenge proposed by the ICCBR. The aim is to suggest a tasty cold sandwich recipe that matches a user query including a set of desired ingredients and avoiding unwanted ones by means of a CBR system.

## Contributors:
* Aleix Solanes (aleix.solanes@est.fib.upc.edu)
* Pablo Martínez (pablo.martinez.martinez@est.fib.upc.edu)
* Vicent Roig (vicent.roig@est.fib.upc.edu)
* Christoph Weilenmann (christoph.weilenmann@est.fib.upc.edu)


## Sandwich Challenge Website ###
* http://ccc2015.loria.fr/?id=sandwichChallenge

### Input:
1. Desired ingredients
2. Undesired ingredients

### API Wiki Knowledge Base ###
* https://www.mediawiki.org/wiki/Developer_hub
* https://github.com/RDFLib/rdflib